package pashock.vkgallery.views.other;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Map;

/**
 * Created by Pashock on 18.03.2017.
 */

public class LoginVKWebView extends WebView {

    /**
     * Случашель изменений URL авторизации
     */
    public interface OnUrlChangeListener {
        /**
         * @param resultUrl URL после изменения
         */
        void onUrlChange(String resultUrl);
    }

    private OnUrlChangeListener urlChangeListener = null;


    /**
     * Constructs a new WebView with layout parameters.
     *
     * @param context a Context object used to access application assets
     */
    public LoginVKWebView(Context context) {
        super(context);
    }

    /**
     * Constructs a new WebView with layout parameters.
     *
     * @param context a Context object used to access application assets
     * @param attrs   an AttributeSet passed to our parent
     */
    public LoginVKWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setWebViewClient(new MyWebViewClient());
    }

    /**
     * Constructs a new WebView with layout parameters and a default style.
     *
     * @param context      a Context object used to access application assets
     * @param attrs        an AttributeSet passed to our parent
     * @param defStyleAttr an attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     */
    public LoginVKWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Constructs a new WebView with layout parameters and a default style.
     *
     * @param context      a Context object used to access application assets
     * @param attrs        an AttributeSet passed to our parent
     * @param defStyleAttr an attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     *                     the view. Can be 0 to not look for defaults.
     * @param defStyleRes  a resource identifier of a style resource that
     *                     supplies default values for the view, used only if
     *                     defStyleAttr is 0 or can not be found in the theme. Can be 0
     */
    public LoginVKWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void loadUrl(String url) {
        loadUrl(url, null);
    }

    @Override
    public void loadUrl(String url, Map<String, String> additionalHttpHeaders) {
        super.loadUrl(url, additionalHttpHeaders);
        // генерим событие, как только загружаем новый URL
        if (urlChangeListener != null) {
            urlChangeListener.onUrlChange(url);
        }
    }


    public void setOnUrlChangeListener(OnUrlChangeListener listener) {
        this.urlChangeListener = listener;
    }





    private class MyWebViewClient extends WebViewClient
    {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

}
