package pashock.vkgallery.views.interfaces;

import android.support.annotation.Nullable;

import org.json.JSONObject;

/**
 * Created by Pashock on 28.05.2017. <br>
 * Соглашение о том, какие методы должно реализовать View, отображающее маленькую галерею
 */
public interface MosaicViewInterface {

    /**
     *  Показать сообщение о том, что пользователь успешно залогинился
     */
    void showSuccessAuthMessage();

    /**
     * Показать сообщение об ошибке
     */
    void showErrorMessage();

    /**
     * Показать сообщение о том, что необходимо залогиниться
     */
    void showErrorNeedAuth();

    /**
     * Отобразить фото, переданные в формате JSON. <br>
     * Чтобы сбросить отображение фото, нужно передать null.
     */
    void showMosaicPhotos(@Nullable JSONObject jsonPhotos);

    /**
     * Пройти на View авторизации
     */
    void doAuthorization();

    /**
     * Сделать действия по разлогину
     */
    void doLogout();

}
