package pashock.vkgallery.views.interfaces;

import android.support.annotation.Nullable;

import org.json.JSONObject;

import java.util.List;

import pashock.vkgallery.model.PhotoItem;

/**
 * Created by Pashock on 05.06.2017.
 */

public interface MosaicFragmentInterface {

    /**
     * Преобразовать новые фото, переданные в формате JSON. <br>
     * Чтобы сбросить отображение фото, нужно передать null.
     * @param jsonPhotos
     */
    void convertJsonToPhotos(@Nullable JSONObject jsonPhotos);

    /**
     * Отобразить фото, сконвертированные в методе {@link MosaicFragmentInterface#convertJsonToPhotos(JSONObject)}
     * @param photos
     */
    void showPhotos(List<PhotoItem> photos);

}
