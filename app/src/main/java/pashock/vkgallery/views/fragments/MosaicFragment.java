package pashock.vkgallery.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pashock.vkgallery.R;
import pashock.vkgallery.presenters.MosaicFragmentPresenter;
import pashock.vkgallery.presenters.MosaicFragmentPresenterInterface;
import pashock.vkgallery.views.activities.GalleryActivity;
import pashock.vkgallery.utils.Consts;
import pashock.vkgallery.model.PhotoItem;
import pashock.vkgallery.views.adapters.RecyclerMosaicAdapter;
import pashock.vkgallery.views.interfaces.MosaicFragmentInterface;

/**
 * Фрагмент, отображающий маленькую галерею <br>
 * Для того, чтобы отобразить фотографии,
 * надо передать JSON с фото в метод: {@link MosaicFragment#convertJsonToPhotos}() <br>
 * Чтобы сбросить отображение фото, можно передать null.
 */
public class MosaicFragment extends Fragment implements View.OnClickListener,
                                                        MosaicFragmentInterface {

    private final int REQUEST_CODE_SHOW_BIG_PHOTOS = 2;

    private RecyclerView mRecyclerView;
    private RecyclerMosaicAdapter mRecyclerAdapter;
    /** информация о фотографиях в формате JSON */
    private JSONObject mjsonPhotos;
    /** список фото */
    private ArrayList<PhotoItem> mPhotoItems;

    private MosaicFragmentPresenterInterface mPresenter;


    public MosaicFragment() {
        mPresenter = new MosaicFragmentPresenter(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
//        if (getArguments() != null) {
//            //mParam1 = getArguments().getString(ARG_PARAM1);
//            // TODO: может лучше сделать через агрументы???
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_mosaic, container, false);
        mRecyclerView = (RecyclerView)root.findViewById(R.id.rv_mosaic);
        GridLayoutManager llmanager = new GridLayoutManager(getContext(), 3);
        mRecyclerView.setLayoutManager(llmanager);
        mPhotoItems = new ArrayList<>();
        mRecyclerAdapter = new RecyclerMosaicAdapter(getContext(), getLoaderManager());
        mRecyclerAdapter.setListItems(mPhotoItems);
        mRecyclerAdapter.setOnClickListener(this);
        mRecyclerView.setAdapter(mRecyclerAdapter);
        return root;
    }

    /**
     * Преобразовать новые фото, переданные в формате JSON. <br>
     * Чтобы сбросить отображение фото, нужно передать null.
     * @param jsonPhotos
     */
    public void convertJsonToPhotos(@Nullable JSONObject jsonPhotos) {
        mjsonPhotos = jsonPhotos; // TODO: может лучше не сохранять весь JSON ??????????????????????
        mPresenter.convertJsonAndShowPhotos(jsonPhotos);
    }

    @Override
    public void showPhotos(List<PhotoItem> photos) {
        mPhotoItems.clear();
        mPhotoItems = (ArrayList<PhotoItem>) photos;
        mRecyclerAdapter.setListItems(mPhotoItems);
        mRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getContext(), GalleryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Consts.S_POSITION, mRecyclerView.getChildLayoutPosition(v));
        bundle.putString(Consts.S_JSONPHOTOS, mjsonPhotos.toString());
        intent.putExtra(Consts.S_IMAGESINFO, bundle);
        startActivityForResult(intent, REQUEST_CODE_SHOW_BIG_PHOTOS);
    }


}
