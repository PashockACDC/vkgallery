package pashock.vkgallery.views.adapters;

import android.content.Context;
import android.support.v4.content.Loader;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.LinkedList;
import java.util.List;

import pashock.vkgallery.R;
import pashock.vkgallery.model.PhotoItem;
import pashock.vkgallery.utils.PhotoLoader;
import pashock.vkgallery.utils.Consts;

/**
 * Created by Pashock on 05.05.2017.
 */
public class RecyclerMosaicAdapter extends RecyclerView.Adapter<RecyclerMosaicAdapter.MosaicViewHolder>
                                   implements LoaderManager.LoaderCallbacks<Bitmap>{

    private List<PhotoItem> mImageItems;
    private MosaicViewHolder[] mHolders;
    private Context mContext;
    private LoaderManager mLoaderManager;

    private View.OnClickListener mOnClickListener;



    class MosaicViewHolder extends RecyclerView.ViewHolder {
        ImageView mImagePreview;
        ProgressBar mProgBar;
        Button mButtonRetry;

        MosaicViewHolder(View itemView) {
            super(itemView);
            mImagePreview = (ImageView)itemView.findViewById(R.id.iv_mosaic_item);
            mProgBar = (ProgressBar)itemView.findViewById(R.id.progress_bar_2);
            mButtonRetry = (Button)itemView.findViewById(R.id.btn_retry_mosaic);
        }
    }


    public RecyclerMosaicAdapter(Context context, LoaderManager loaderManager) {
        mContext = context;
        mLoaderManager = loaderManager;
        mImageItems = new LinkedList<>();
        mHolders = null;
    }

    public void setListItems(List<PhotoItem> items) {
        mImageItems = items;
        mHolders = new MosaicViewHolder[mImageItems.size()];
    }


    /**
     * Called when RecyclerView needs a new {@link MosaicViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(MosaicViewHolder, int)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(MosaicViewHolder, int)
     */
    @Override
    public MosaicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mosaic_item, parent, false);
        v.setOnClickListener(mOnClickListener);
        return new MosaicViewHolder(v);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link MosaicViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link MosaicViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(MosaicViewHolder, int)} instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(MosaicViewHolder holder, int position) {
        Bundle params = new Bundle();
        mHolders[position] = holder;
        params.putString(Consts.S_URL_FOR_LOAD, mImageItems.get(position).getSmallPictureLink());
        PhotoLoader l = (PhotoLoader)mLoaderManager.initLoader(position, params, this);
        try {
            l.forceLoad();
        } catch (Exception e) {
            e.printStackTrace();
            mLoaderManager.restartLoader(position, params, this);
        }
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return mImageItems.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public void setOnClickListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }




    @Override
    public Loader<Bitmap> onCreateLoader(int id, Bundle params) {
        Loader<Bitmap> loader = null;
        if (id < mImageItems.size()) {////////////////////////////////////
            loader = new PhotoLoader(mContext, params);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(final Loader<Bitmap> loader, Bitmap data) {
        int n = loader.getId();
        mHolders[n].mProgBar.setVisibility(View.GONE);
        mHolders[n].mButtonRetry.setVisibility(View.GONE);
        if (data != null ) {
            mHolders[n].mImagePreview.setVisibility(View.VISIBLE);
            mHolders[n].mImagePreview.setImageDrawable(new BitmapDrawable(mContext.getResources(), data));
            mLoaderManager.destroyLoader(n);
        }
        else {
            mHolders[n].mProgBar.setVisibility(View.VISIBLE);
            mHolders[n].mButtonRetry.setVisibility(View.VISIBLE);
            mHolders[n].mButtonRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLoaderReset(loader);
                }
            });
            mHolders[n].mImagePreview.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoaderReset(Loader<Bitmap> loader) {
        int n = loader.getId();
        mHolders[n].mProgBar.setVisibility(View.VISIBLE);
        mHolders[n].mButtonRetry.setVisibility(View.GONE);
        mHolders[n].mImagePreview.setVisibility(View.GONE);
        Bundle params = new Bundle();
        params.putString(Consts.S_URL_FOR_LOAD, mImageItems.get(n).getSmallPictureLink());
        mLoaderManager.restartLoader(n, params, this);
    }

    @Override
    public void onViewDetachedFromWindow(MosaicViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Loader<Bitmap> loader = mLoaderManager.getLoader(holder.getAdapterPosition());
        if (loader != null) {
            loader.stopLoading();
            loader.cancelLoad();
        }
        mLoaderManager.destroyLoader(holder.getLayoutPosition());
    }

}
