package pashock.vkgallery.views.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import pashock.vkgallery.R;
import pashock.vkgallery.model.PhotoItem;
import pashock.vkgallery.utils.Consts;
import pashock.vkgallery.utils.PhotoLoader;

/**
 * Created by Pashock on 11.05.2017.
 */

public class FullScreenImageAdapter extends PagerAdapter
                                    implements LoaderManager.LoaderCallbacks<Bitmap>{

    private Context mContext;
    private LoaderManager mLoaderManager;
    private List<PhotoItem> mItems;
    private Holder[] mHolders;


    public FullScreenImageAdapter(Context context, LoaderManager loaderManager) {
        mContext = context;
        mLoaderManager = loaderManager;
        mItems = null;
        mHolders = null;
    }


    public void setListItems(List<PhotoItem> items) {
        mItems = items;
        mHolders = new Holder[mItems.size()];
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    private class Holder {
        View mViewLayout;
        ImageView mImageView;
        ProgressBar mProgBar;
        TextView mTextError;
        Button mButtonRetry;

        Holder (View viewContainer) {
            mViewLayout = viewContainer;
            mProgBar = (ProgressBar)mViewLayout.findViewById(R.id.progbar_loadSinglePhoto);
            mButtonRetry = (Button)mViewLayout.findViewById(R.id.btn_retry);
            mImageView = (ImageView)mViewLayout.findViewById(R.id.imgDisplay);
            mTextError = (TextView)mViewLayout.findViewById(R.id.tv_error);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mItems.size();
    }

    /**
     * Determines whether a page View is associated with a specific key object
     * as returned by {@link #instantiateItem(ViewGroup, int)}. This method is
     * required for a PagerAdapter to function properly.
     *
     * @param view   Page View to check for association with <code>object</code>
     * @param object Object to check for association with <code>view</code>
     * @return true if <code>view</code> is associated with the key object <code>object</code>
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.content_gallery, container, false);

        TextView tvDescription = (TextView) viewLayout.findViewById(R.id.tv_description);
        tvDescription.setText(mItems.get(position).getTextInfo());

        mHolders[position] = new Holder(viewLayout);
        Bundle params = new Bundle();
        params.putString(Consts.S_URL_FOR_LOAD, mItems.get(position).getBigPictureLink());
        PhotoLoader l = (PhotoLoader)mLoaderManager.initLoader(position, params, this);
        l.forceLoad();

        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
        Loader<Bitmap> loader = mLoaderManager.getLoader(position);
        if (loader != null) {
            loader.stopLoading();
            loader.cancelLoad();
        }
        mLoaderManager.destroyLoader(position);
    }


    @Override
    public Loader<Bitmap> onCreateLoader(int id, Bundle params) {
        Loader<Bitmap> loader = null;
        if (id < mItems.size()) {
            loader = new PhotoLoader(mContext, params);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(final Loader<Bitmap> loader, Bitmap data) {
        int n = loader.getId();
        mHolders[n].mProgBar.setVisibility(View.GONE);
        mHolders[n].mTextError.setVisibility(View.GONE);
        mHolders[n].mButtonRetry.setVisibility(View.GONE);
        if (data != null) {
            mHolders[n].mImageView.setVisibility(View.VISIBLE);
            mHolders[n].mImageView.setImageBitmap(data);
            mLoaderManager.destroyLoader(n);
        } else {
            mHolders[n].mTextError.setVisibility(View.VISIBLE);
            mHolders[n].mButtonRetry.setVisibility(View.VISIBLE);
            mHolders[n].mButtonRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLoaderReset(loader);
                }
            });
        }
    }

    @Override
    public void onLoaderReset(final Loader<Bitmap> loader) {
        int n = loader.getId();
        mHolders[n].mProgBar.setVisibility(View.VISIBLE);
        mHolders[n].mTextError.setVisibility(View.GONE);
        mHolders[n].mButtonRetry.setVisibility(View.GONE);
        mHolders[n].mImageView.setVisibility(View.GONE);
        Bundle params = new Bundle();
        params.putString(Consts.S_URL_FOR_LOAD, mItems.get(n).getBigPictureLink());
        mLoaderManager.restartLoader(n, params, this);
    }

}
