package pashock.vkgallery.views.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pashock.vkgallery.presenters.MosaicPresenter;
import pashock.vkgallery.presenters.MosaicPresenterInterface;
import pashock.vkgallery.views.fragments.MosaicFragment;
import pashock.vkgallery.R;
import pashock.vkgallery.utils.Consts;
import pashock.vkgallery.views.interfaces.MosaicViewInterface;

/**
 * Created by PashockACDC on 21.03.2017. <br>
 * Главное Activity, которое отображает маленькую галерею, если пользователь залогинен
 */
public class MosaicActivity extends BaseActivity implements MosaicViewInterface {

    private final int REQUEST_CODE_AUTH = 1;

    private String mAccessToken;
    private String mUserId;
    private JSONObject mJsonPhotos;

    @InjectView(R.id.tv_ok)
    TextView mtvOK;
    @InjectView(R.id.progress_bar_1)
    ProgressBar mProgressBar1;

    private MosaicFragment mMosaicFragment;

    private MosaicPresenterInterface mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mosaic);

        ButterKnife.inject(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mMosaicFragment = (MosaicFragment)fragmentManager.findFragmentById(R.id.frag_mosaic);
        if (mPresenter == null)
            mPresenter = new MosaicPresenter(mContext, this);
        if (savedInstanceState != null) {
            try {
                String s = savedInstanceState.getString(Consts.S_JSONPHOTOS);
                if (s != null) {
                    mJsonPhotos = new JSONObject(s);
                    mMosaicFragment.convertJsonToPhotos(mJsonPhotos);
                    showSuccessAuthMessage();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            mPresenter.navigateToAuthorization();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mtvOK.getText().toString().contains(getResources().getString(R.string.auth_success))) {
            menu.findItem(R.id.menu_auth).setEnabled(false);
            menu.findItem(R.id.menu_logout).setEnabled(true);
        } else {
            menu.findItem(R.id.menu_auth).setEnabled(true);
            menu.findItem(R.id.menu_logout).setEnabled(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_auth:
                mPresenter.navigateToAuthorization();
                break;

            case R.id.menu_logout:
                mPresenter.doLogout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mJsonPhotos != null) {
            outState.putString(Consts.S_JSONPHOTOS, mJsonPhotos.toString());
        }
    }



    @Override
    public void showSuccessAuthMessage() {
        mtvOK.setText(R.string.auth_success);
        mtvOK.setTextColor(getResources().getColor(R.color.colorPrimary));
        mtvOK.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorNeedAuth() {
        mtvOK.setText(R.string.auth_need);
        mtvOK.setTextColor(getResources().getColor(R.color.colorAccent));
        mtvOK.setVisibility(View.VISIBLE);
        mMosaicFragment.convertJsonToPhotos(null);
    }

    @Override
    public void showErrorMessage() {
        mtvOK.setText(R.string.err_response);
        mtvOK.setTextColor(getResources().getColor(R.color.colorAccent));
        mtvOK.setVisibility(View.VISIBLE);
        mMosaicFragment.convertJsonToPhotos(null);
    }


    @Override
    public void showMosaicPhotos(JSONObject jsonPhotos) {
        mJsonPhotos = jsonPhotos;
        mMosaicFragment.convertJsonToPhotos(mJsonPhotos);
    }


    @Override
    public void doAuthorization() {
        mProgressBar1.setVisibility(View.VISIBLE);
        if (isNetworkAvailable()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, REQUEST_CODE_AUTH);
        }
        else {
            showErrorDialog(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    doAuthorization();
                }
            });
        }
    }

    @Override
    public void doLogout() {
        mAccessToken = null;
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Consts.S_LOGOUT, "");
        startActivityForResult(intent, REQUEST_CODE_AUTH);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mProgressBar1.setVisibility(View.GONE);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQUEST_CODE_AUTH:
                    mAccessToken = data.getStringExtra(Consts.S_ACCESS_TOKEN);
                    mUserId = data.getStringExtra(Consts.S_USER_ID);
                    showSuccessAuthMessage();
                    mPresenter.doRequestingPhotos(getSupportLoaderManager(), mUserId, mAccessToken);
                    break;

                default:
                    // Is it Possible!?
                    break;
            }
        }
        else {
            mAccessToken = null;
            showErrorNeedAuth();
            mMosaicFragment.convertJsonToPhotos(null);
        }
        invalidateOptionsMenu();
    }

}
