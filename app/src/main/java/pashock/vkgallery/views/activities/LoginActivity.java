package pashock.vkgallery.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ProgressBar;

import java.net.MalformedURLException;
import java.net.URL;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pashock.vkgallery.R;
import pashock.vkgallery.utils.Consts;
import pashock.vkgallery.utils.Utils;
import pashock.vkgallery.views.other.LoginVKWebView;

/**
 * Created by PashockACDC on 09.03.2017.
 */

public class LoginActivity extends BaseActivity {

    private static final String URL_AUTHORIZE = "http://oauth.vk.com/oauth/authorize";
    private static final String URL_REDIRECT = "http://oauth.vk.com/blank.html";

    @InjectView(R.id.webview_auth)
    LoginVKWebView mWebViewAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.inject(this);

        mWebViewAuth.setOnUrlChangeListener(urlChangeListener);

        String logout = getIntent().getStringExtra(Consts.S_LOGOUT);
        if (logout != null) {
            clearCookies(mContext);
            finish();
        } else {
            final String url = URL_AUTHORIZE +
                    "?client_id=" + Utils.S_CLIENT_ID +
                    "&display=mobile" +
                    "&redirect_uri=" + URL_REDIRECT +
                    "&scope=photos" +
                    "&response_type=token" +
                    "&v=5.62" +
                    "&client_secret=" + Utils.S_CLIENT_SECRET +
                    "&state=123456";
            if (isNetworkAvailable()) {
                mWebViewAuth.loadUrl(url);
            }
        }
    }

    /**
     * Обработка изменений URL
     */
    private LoginVKWebView.OnUrlChangeListener urlChangeListener = new LoginVKWebView.OnUrlChangeListener() {
        @Override
        public void onUrlChange(String resultUrl) {
            if (resultUrl.contains(Consts.S_ACCESS_TOKEN) && resultUrl.contains(Consts.S_USER_ID)) {
                Intent intent = new Intent();
                String accessToken = Utils.getAccessTokenFromUrl(resultUrl);
                String userId = Utils.getUserIdFromUrl(resultUrl);
                intent.putExtra(Consts.S_ACCESS_TOKEN, accessToken);
                intent.putExtra(Consts.S_USER_ID, userId);
                setResult(RESULT_OK, intent);
                finish();
            }
            else if (resultUrl.contains("user_denied")) {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    };


    /**
     * Разрешаем WebView пользоваться кнопкой "Назад"
     */
    @Override
    public void onBackPressed() {
        if(mWebViewAuth.canGoBack()) {
            mWebViewAuth.goBack();
        } else {
            super.onBackPressed();
        }
    }

    public void clearCookies(Context context)
    {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else
        {
            CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

}
