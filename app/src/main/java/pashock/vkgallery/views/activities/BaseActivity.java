package pashock.vkgallery.views.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import pashock.vkgallery.R;

/**
 * Created by Pashock on 24.04.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected Handler handler;
    protected Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler = new Handler(Looper.getMainLooper());
        mContext = this;
    }

    /**
     * Проверка доступности интернета
     */
    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /**
     * Показываем окошко, что интернетов нет
     * @param retryAction действие для повтора
     */
    protected void showErrorDialog(final DialogInterface.OnClickListener retryAction) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext,
                        R.style.AppCompatAlertDialogStyle);
                builder.setTitle(R.string.err_noInernets_title);
                builder.setMessage(R.string.err_noInernets_message);
                builder.setPositiveButton(R.string.err_noInernets_retry, retryAction);
                builder.setNegativeButton(R.string.err_noInernets_notRetry, null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

}
