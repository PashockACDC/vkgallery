package pashock.vkgallery.views.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pashock.vkgallery.R;
import pashock.vkgallery.utils.Consts;
import pashock.vkgallery.model.PhotoItem;
import pashock.vkgallery.utils.Utils;
import pashock.vkgallery.views.adapters.FullScreenImageAdapter;

public class GalleryActivity extends BaseActivity {

    @InjectView(R.id.viewPager)
    ViewPager mViewPager;

    int position = 0;

    private FullScreenImageAdapter mFullAdapter;
    /** информация о фотографиях */
    private ArrayList<PhotoItem> mPhotoInfos;
    /** информация о фотографиях в формате JSON */
    JSONObject mjsonPhotos;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        ButterKnife.inject(this);

        Bundle bundle = getIntent().getBundleExtra(Consts.S_IMAGESINFO);
        if (bundle != null) {
            position = bundle.getInt(Consts.S_POSITION);
            try {
                mjsonPhotos = new JSONObject(bundle.getString(Consts.S_JSONPHOTOS));
                mPhotoInfos = Utils.getPhotosInfo(mjsonPhotos);
                ArrayList<PhotoItem> items = new ArrayList<>();
                for (PhotoItem pi : mPhotoInfos) {
                    items.add(new PhotoItem(pi.getTextInfo(),
                                            pi.getSmallPictureLink(),
                                            pi.getBigPictureLink()));
                }
                mFullAdapter = new FullScreenImageAdapter(mContext, getSupportLoaderManager());
                mFullAdapter.setListItems(items);
                mViewPager.setAdapter(mFullAdapter);
                mViewPager.setCurrentItem(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setResult(RESULT_OK);
    }



}
