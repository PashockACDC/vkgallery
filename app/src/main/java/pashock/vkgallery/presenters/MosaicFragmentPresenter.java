package pashock.vkgallery.presenters;

import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pashock.vkgallery.model.PhotoItem;
import pashock.vkgallery.utils.Utils;
import pashock.vkgallery.views.interfaces.MosaicFragmentInterface;

/**
 * Created by Pashock on 05.06.2017.
 */

public class MosaicFragmentPresenter implements MosaicFragmentPresenterInterface {

    private MosaicFragmentInterface mFragment;


    public MosaicFragmentPresenter(MosaicFragmentInterface fragment) {
        mFragment = fragment;
    }


    @Override
    public void convertJsonAndShowPhotos(@Nullable JSONObject jsonPhotos) {
        ArrayList<PhotoItem> res = new ArrayList<>();
        if (jsonPhotos != null) {
            try {
                ArrayList<PhotoItem> temp = Utils.getPhotosInfo(jsonPhotos);
                if (temp != null) {
                    for (PhotoItem mPhotoInfo : temp) {
                        res.add(new PhotoItem(mPhotoInfo.getTextInfo(),
                                              mPhotoInfo.getSmallPictureLink(),
                                              mPhotoInfo.getBigPictureLink()));
                    }
                }
            } catch (JSONException e) {
                // TODO: подумать, что делать, если хлопнет при обработке JSON'а
                e.printStackTrace();
            }
        }
        mFragment.showPhotos(res);
    }
}
