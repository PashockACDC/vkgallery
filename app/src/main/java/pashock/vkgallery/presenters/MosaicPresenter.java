package pashock.vkgallery.presenters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import org.json.JSONObject;

import pashock.vkgallery.utils.Consts;
import pashock.vkgallery.utils.GettingJSONPhotosLoader;
import pashock.vkgallery.views.interfaces.MosaicViewInterface;

/**
 * Created by Pashock on 28.05.2017. <br>
 * Посредник, который позволяет View (реализующей {@link MosaicViewInterface}): <br>
 *     - залогиниться пользователю vk.com; <br>
 *     - после успешной авторизации показать превью доступных фото; <br>
 *     - перейти на большую галерею после клика на превью (реализуется сооветствующим фрагментом); <br>
 *     - разлогиниться.
 */
public class MosaicPresenter implements LoaderManager.LoaderCallbacks<JSONObject>,
                                        MosaicPresenterInterface {

    private static final int LOADER_GET_PHOTOS_INFO_ID = 1;

    private MosaicViewInterface mMosaicView;
    private Context mContext;
    private Loader<JSONObject> mLoader;

    public MosaicPresenter (Context context, MosaicViewInterface view) {
        mContext = context;
        mMosaicView = view;
    }

    /**
     * Сделать запросить все фото пользователя
     * @param loaderManager
     * @param userId идентификатор пользователя или сообщества, фотографии которого нужно получить.
     *               Если сообщество, то указывается со знаком "-"
     * @param accessToken специальный ключ доступа пользователя
     */
    @Override
    public void doRequestingPhotos(@NonNull LoaderManager loaderManager, String userId, String accessToken) {
        Bundle params = new Bundle();
        params.putString(Consts.S_ACCESS_TOKEN, accessToken);
        params.putString(Consts.S_USER_ID, userId);
        if (mLoader == null) {
            mLoader = loaderManager.initLoader(LOADER_GET_PHOTOS_INFO_ID, params, this);
        }
        else {
            mLoader = loaderManager.restartLoader(LOADER_GET_PHOTOS_INFO_ID, params, this);
        }
        mLoader.forceLoad();
    }

    /**
     * Пройти на View авторизации
     */
    @Override
    public void navigateToAuthorization() {
        mMosaicView.doAuthorization();
    }

    /**
     * Сделать действия по разлогину
     */
    @Override
    public void doLogout() {
        mMosaicView.doLogout();
    }




    @Override
    public Loader<JSONObject> onCreateLoader(int id, Bundle params) {
        Loader<JSONObject> loader = null;
        if (id == LOADER_GET_PHOTOS_INFO_ID) {
            loader = new GettingJSONPhotosLoader(mContext, params);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<JSONObject> loader, JSONObject data) {
        if (data.toString().contains("error")) {
            mMosaicView.showErrorMessage();
        } else {
            mMosaicView.showSuccessAuthMessage();
            mMosaicView.showMosaicPhotos(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<JSONObject> loader) {

    }

}
