package pashock.vkgallery.presenters;

import android.support.annotation.Nullable;

import org.json.JSONObject;

/**
 * Created by Pashock on 05.06.2017.
 */

public interface MosaicFragmentPresenterInterface {

    void convertJsonAndShowPhotos(@Nullable JSONObject jsonPhotos);

}
