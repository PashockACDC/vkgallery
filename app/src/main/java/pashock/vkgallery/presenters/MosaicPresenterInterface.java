package pashock.vkgallery.presenters;

import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;

import pashock.vkgallery.views.interfaces.MosaicViewInterface;

/**
 * Created by Pashock on 01.06.2017. <br>
 * Соглашение о том, какие методы должен реализовать {@link MosaicPresenter},
 * чтобы управлять view, реализующей {@link MosaicViewInterface}.
 */
public interface MosaicPresenterInterface {

    /**
     * Сделать запросить все фото пользователя
     * @param loaderManager
     * @param userId идентификатор пользователя или сообщества, фотографии которого нужно получить.
     *               Если сообщество, то указывается со знаком "-"
     * @param accessToken специальный ключ доступа пользователя
     */
    void doRequestingPhotos(@NonNull LoaderManager loaderManager, String userId, String accessToken);

    /**
     * Пройти на View авторизации
     */
    void navigateToAuthorization();

    /**
     * Сделать действия по разлогину
     */
    void doLogout();
}
