package pashock.vkgallery.model;


/**
 * Created by Pashock on 31.03.17.
 * Простой класс, представяющий собой информацию, нужную для отображения фото
 */
public class PhotoItem {

    private String mTextInfo;
    private String mSmallPictureUrl;
    private String mBigPictureUrl;

    /**
     *
     * @param textInfo описание фото
     * @param smallPictureUrl ссылка на маленькую фотографию
     * @param bigPictureUrl ссылка на большую фотографию
     */
    public PhotoItem(String textInfo, String smallPictureUrl, String bigPictureUrl) {
        this.mTextInfo = textInfo;
        this.mSmallPictureUrl = smallPictureUrl;
        this.mBigPictureUrl = bigPictureUrl;
    }


    public String getTextInfo() {
        return mTextInfo;
    }

    public String getSmallPictureLink() {
        return mSmallPictureUrl;
    }

    public String getBigPictureLink() {
        return mBigPictureUrl;
    }

    public void setTextInfo(String descr) {
        mTextInfo = descr;
    }

    public void setUrlSmall(String url) {
        this.mSmallPictureUrl = url;
    }

    public void setUrlBig(String url) {
        this.mBigPictureUrl = url;
    }

    @Override
    public String toString() {
        return mTextInfo + '\n' +
               mSmallPictureUrl + '\n' +
               mBigPictureUrl + '\n';
    }

}
