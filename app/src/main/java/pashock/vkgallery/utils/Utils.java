package pashock.vkgallery.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import pashock.vkgallery.model.PhotoItem;

/**
 * Created by PashockACDC on 09.03.2017.
 */

public class Utils {

    public static final String URL_PHOTOS_GETALL = "https://api.vk.com/method/photos.getAll";
    public static final String S_CLIENT_SECRET = "wSsYuZTQNlvqHVdKJw9h";
    public static final String S_CLIENT_ID = "5921175";

    /**
     * Конвертируем входящий поток в String
     * @param in
     * @return
     * @throws IOException
     */
    public static String convertStreamToString(InputStream in) throws IOException {
        int i = 1;
        List<Byte> bytes = new LinkedList<>();
        while (i > 0) {
            i = in.read();
            bytes.add( (byte)i );
        }
        byte[] b = new byte[bytes.size() - 1];
        for (int j = 0; j < bytes.size() - 1; j++) {
            byte t = bytes.get(j);
            b[j] = t;
        }
        return new String(b);
    }


    /**
     * Получить user_id из url
     * @return
     */
    @NonNull
    public static String getUserIdFromUrl(@NonNull String url) {
        return url.substring(url.indexOf("user_id=") + 8, url.indexOf("&state"));
    }


    /**
     * Получить access_token из url
     * @return
     */
    @NonNull
    public static String getAccessTokenFromUrl(@NonNull String url) {
        return url.substring(url.indexOf("=") + 1, url.indexOf("&"));
    }


    /**
     * Преобразует JSON-ответ в массив объектов, представляющих информацию о фотографиях
     * @param json ответ в формате JSON, приходящий от API
     * @return null, если кол-во фото == 0
     * @throws JSONException
     */
    @Nullable
    public static ArrayList<PhotoItem> getPhotosInfo(@NonNull JSONObject json) throws JSONException {
        JSONArray jsonArray = json.getJSONArray("response");

        int countPhotos = Integer.parseInt(jsonArray.get(0).toString());
        if (countPhotos == 0)
            return null;

        ArrayList<PhotoItem> res = new ArrayList<>();
        for (int i = 1; i <= jsonArray.length() - 1; i++) {
            try {
                res.add(new PhotoItem((new JSONObject(jsonArray.get(i).toString())).get("text").toString(),
                                      (new JSONObject(jsonArray.get(i).toString())).get("src_small").toString(),
                                      (new JSONObject(jsonArray.get(i).toString())).get("src_big").toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

}
