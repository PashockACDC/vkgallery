package pashock.vkgallery.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.WorkerThread;
import android.util.LruCache;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by Pashock on 30.03.17. <br>
 * Usage: <br>
 * <code>
 *     Bitmap bmp = PictureLoaderUtil.getInstance().loadBitmapByUrl(mContext, "http://example.com/example.jpg");
 * </code>
 */
class PictureLoaderUtil {

    private static PictureLoaderUtil instance;
    private LruCache<String, Bitmap> mMemoryCache;

    private PictureLoaderUtil() {
        // Get max available VM memory, exceeding this amount will throw an OutOfMemoryException.
        // Stored in kilobytes as LruCache takes an int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }


    static synchronized PictureLoaderUtil getInstance() {
        if (instance == null) {
            instance = new PictureLoaderUtil();
        }
        return instance;
    }



    @WorkerThread
    Bitmap loadBitmapByUrl(String url) throws ExecutionException, InterruptedException {
        String bmpKey = Uri.parse(url).getLastPathSegment();
        Bitmap bmp = getBitmapFromMemCache(bmpKey);
        if (bmp == null) {
            InputStream in = null;
            try {
                in = new URL(url).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            addBitmapToMemoryCache(bmpKey, bmp);
        }
        return bmp;
    }


    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

}
