package pashock.vkgallery.utils;

import android.os.AsyncTask;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.Loader;

/**
 * Created by Pashock on 01.06.2017.
 */

public class PhotoLoader extends Loader<Bitmap> {

    private String mUrlForLoad;
    private InnerAsyncLoader mInnerLoader;


    public PhotoLoader(Context context, Bundle params) {
        super(context);
        if (params != null) {
            mUrlForLoad = params.getString(Consts.S_URL_FOR_LOAD);
        }
    }


    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        // TODO: ПОДУМАТЬ, НУЖНЫ ЛИ ДЕЙСТВИЯ НИЖЕ
        mInnerLoader = new InnerAsyncLoader();
        try {
            mInnerLoader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mUrlForLoad);
        }
        catch (Exception e) {
            deliverCancellation();
            e.printStackTrace();
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        if (mInnerLoader != null)
            mInnerLoader.cancel(true);
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        if (mInnerLoader != null) {
            mInnerLoader.cancel(true);
        }
        mInnerLoader = new InnerAsyncLoader();
        try {
            mInnerLoader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mUrlForLoad);
        }
        catch (Exception e) {
            deliverCancellation();
            e.printStackTrace();
        }
    }



    private class InnerAsyncLoader extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap res = null;
            try {
                res = PictureLoaderUtil.getInstance().loadBitmapByUrl(mUrlForLoad);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            deliverResult(bitmap);
        }
    }

}
