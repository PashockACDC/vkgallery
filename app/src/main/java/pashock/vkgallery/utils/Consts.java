package pashock.vkgallery.utils;

/**
 * Created by Pashock on 15.05.2017.
 */

public class Consts {

    public static final String S_JSONPHOTOS = "JSONPhotos";
    public static final String S_IMAGESINFO = "ImagesInfo";
    public static final String S_POSITION = "position";
    public static final String S_LOGOUT = "logout";
    public static final String S_ACCESS_TOKEN = "access_token";
    public static final String S_USER_ID = "user_id";
    public static final String S_URL_FOR_LOAD = "url_for_load";

}
