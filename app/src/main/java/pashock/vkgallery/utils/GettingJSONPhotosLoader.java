package pashock.vkgallery.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Pashock on 31.05.2017. <br>
 * Лоадер для получения информации о фото для залогинившегося пользователя.
 * В параметры необходимо передать: <br>
 *     - access_token <br>
 *     - user_id
 */
public class GettingJSONPhotosLoader extends AsyncTaskLoader<JSONObject> {

    private String mAccessToken;
    private String mUserId;

    /**
     * Создание лоадера для получения информации о фото для залогинившегося пользователя.
     * @param context
     * @param params В параметры необходимо передать: <br>
     *     - access_token <br>
     *     - user_id
     */
    public GettingJSONPhotosLoader(Context context, Bundle params) {
        super(context);
        if (params != null) {
            mAccessToken = params.getString(Consts.S_ACCESS_TOKEN);
            mUserId = params.getString(Consts.S_USER_ID);
        }
    }

    @Override
    public JSONObject loadInBackground() {
        JSONObject res = null;
        HttpsURLConnection https = null;
        InputStream in = null;
        try {
            URL url = new URL(Utils.URL_PHOTOS_GETALL +
                    "?oauth=2" +
                    "&photos.getAll" +
                    "&owner_id=" + mUserId +
                    "&client_secret=" + Utils.S_CLIENT_SECRET +
                    "&access_token=" + mAccessToken);
            https = (HttpsURLConnection) url.openConnection();
            https.setConnectTimeout(30000);
            in = new BufferedInputStream(https.getInputStream());
            String response = Utils.convertStreamToString(in);
            res = new JSONObject(response);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (https != null)
                https.disconnect();
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return res;
    }
}
